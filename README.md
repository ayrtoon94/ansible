# ansible

Aula da Alura aprendendo do basico a utilização do ansible, aprendendo como distribuir o codigo em varios arquivos diferentes, para minimizar erros e/ou esquecimentos de declarar variaveis.

Anotações importantes:

**ssh-keygen -R [ip_host]** > Para limpar a chave SSH vinculada anteriormente com o host, util para utilizar quando destruimos uma maquina e subimos ela novamente.

<br>

**provisioning.yml** > Arquivo base para realizar as configurações do ansible

**---** > Todo arquivo com extenção .yml inicia com 3 traços

**hosts** > Arquivo para indicar o nome, ip, usuário, chave de acesso aos hosts

**ansible-playbook -i hosts provisioning.yml** - 
Executa o provisioning.yml, para executar dessa maneira, é necessário configurar as opções de acesso no arquivo hosts.

Exemplo:

[database]
192.168.1.51 ansible_user=vagrant ansible_ssh_private_key_file=/root/wordpress_com_ansible/.vagrant/machines/mysql/virtualbox/private_key<br>

**database** > Nome do grupo das maquinas<br>
**192.168.1.51** > O ip do hosts<br>
**ansible_user=vagrant** > O nome do usuário que vai acessar, nesse caso o usuario 'vagrant'<br>
**ansible_ssh_private_key_file=**> Para indicar o caminho da chave privada ssh que será utilizada para acessar o hosts<br>
**/root/wordpress_com_ansible/.vagrant/machines/mysql/virtualbox/private_key** > O caminho que está a chave privada para acessar via SSH.<br>

<br>

**group_vars** > Diretório para declarar variaveis, é possivel criar variaves para um grupo especifico de hosts, ou para todos. 
Para criar um arquivo para todos os grupos, criar um arquivo all.yml dentro desse diretório. <br>
Para criar variaves especificos para um grupo, é necessário criar um arquivo com o nome do grupo que vai receber essas variaveis + .yml [Ex.: database.yml]. As variaveis serão exclusivas para esse grupo, que foi declarado anteriomente no arquivo hosts. <br>
Dentro do arquivo, se declara a variavel, iniciando com o "---", pois é um arquivo .yml, primeiramente declara a variavel, utiliza-se os ":" para indicar que ela vai receber o valor que está logo a frente, como nos exemplos abaixo: <br>

---<br>
wp_username: wordpress_user <br>
wp_db_name: wordpress_db<br>
wp_user_password: 12345<br>
wp_installation_dir: '/var/www/wordpress'<br>
wp_host_ip: '192.168.1.50'<br>
wp_db_ip: '192.168.1.51'<br>

Para chamar a variavel dentro dos arquivos, é utilizado "{{  }}" . [Ex.: "{{ wp_username }}"] <br>


_**Roles**_ <br>
Para melhor organização, separamos todas as configurações em arquivos diferentes, utilizando as 'roles'. Criamos uma configuração para cada grupo de host, criamos um diretório roles, e dentro dele, um outro diretório com o nome do grupo do host, e então, dentro do diretório do grupo, criamos mais alguns subdiretórios, como, defaults, files, handlers, tasks. [Ex: Diretorio_Principal/roles/mysql/defaults] , dentro de cada diretório, podemos criar um arquivo main.yml, para realizar as configurações de cada item.
<br>

No arquivo provisioning.yml, ao inves de programar sua infra nele, você indica as roles que ele irá buscar.
<br>
### Exemplo do provisioning.yml utilizando roles ### <br>

#---
# - hosts: database
#   roles:
#      - mysql
# 
# - hosts: wordpress
#   roles:
#     - wordpress
<br>

No exemplo abaixo temos a configuração de tasks, nesse caso estamos instalando os seguintes pacotes: <br>
- python-mysqldb <br>
- mysql-server-5.6 <br>

**- name** > No momento que estiver provisionando o ansible, é o texto que irá aparecer quando estiver sendo executado essa tarefa. <br>
**apt** > O gerenciado de pacotes que estamos utilizando, por se tratar de um ubunto<br>
**name** > O segundo name indica os pacotes que deseja instalar, são colocado dentro de colchetes[], dentro de aspas simples [''], e separados por vírgula ['', '', '']. <br>
**state** > Informa qual versão será utilizada, nesse caso o _latest_, significa que é o pacote mais atual   <br>
**become** > Informa se a task será iniciado como root <br>

<br>
### Exemplo do arquivo main.yml dentro do diretório tasks ##

#---
#- name: 'Instala pacotes de dependencia do sistema operacional'
#  apt:
#    name: ['python-mysqldb', 'mysql-server-5.6']
#    state: latest
#  become: yes

                     


